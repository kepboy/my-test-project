﻿using System;
using System.Diagnostics;
using System.Linq;
using WizBangTools;

namespace TestConsole
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Hello World");
            Utilities.SayHello();

            if(!Debugger.IsAttached)
                return;

            Console.WriteLine("Press any key to exit");
            Console.ReadKey(true);
        }
    }
}
